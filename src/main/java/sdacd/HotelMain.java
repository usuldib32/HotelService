package sdacd;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HotelMain {
    public static void main(String[] args) {
        Hotel sheraton = new Hotel();
        HotelService sheratonService = new HotelService(sheraton);
        List<Room> allRooms = sheratonService.getRoomsList();
        List<Room> aviableRooms = sheratonService.getAviableRoom();
        List<Guest> guestList = new ArrayList<>();
        guestList.add(new Guest("Gość1","Nazwisko1",LocalDate.of(1980,1,10)));
        guestList.add(new Guest("Gość2","Nazwisko2",LocalDate.of(1995,2,11)));
        guestList.add(new Guest("Gość3","Nazwisko3",LocalDate.of(2015,3,12)));
//        Guest guest1 = new Guest("Gość1","Nazwisko1",LocalDate.of(1980,1,10));
//        Guest guest2 = new Guest("Gość2","Nazwisko",LocalDate.of(1990,2,15));
//        Guest guest3 = new Guest("Gość3","Nazwisko1",LocalDate.of(2010,3,20));

        sheratonService.showRooms(allRooms);
        sheratonService.bookRoom(100,guestList);
      sheratonService.getRoomByNumber(100).showGuestList();
        sheratonService.showRooms(aviableRooms);
        sheratonService.chekOut(100);
        sheratonService.showRooms(aviableRooms);

//        System.out.println("Wolne pokoje.");
//        for(Room room : aviableRooms) {
//            System.out.println(room);
//        }

//        sheratonService.bookRoom(100);
//        System.out.println("Wszystkie pokoje.");
//        for(Room room : allRooms) {
//            System.out.println(room);
//        }
//        sheratonService.chekOut(100);
//        System.out.println("Wszystkie pokoje.");
//        for(Room room : allRooms) {
//            System.out.println(room);
//        }
    }
}
