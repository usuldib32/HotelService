package sdacd;

import java.time.LocalDate;
import java.time.Period;

public class Guest {

    private String name;
    private String surname;
    private LocalDate birthDate;

    public Guest(String name, String surname, LocalDate birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public int getAge(){
        return Period.between(birthDate, LocalDate.now()).getYears();
    }
}
