package sdacd;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Room {
    private int roomNumber;
    private int numberOfPersons;
    private boolean hasBathroom;
    private boolean isAviable;
    private List<Guest> questList;
    private boolean isClean;

    public Room(int roomNumber, int numberOfPersons, boolean hasBathroom, boolean isAviable) {
        this.roomNumber = roomNumber;
        this.numberOfPersons = numberOfPersons;
        this.hasBathroom = hasBathroom;
        this.isAviable = isAviable;
        this.questList = new ArrayList<>();
    }

    public boolean isAviable() {
        return isAviable;
    }

    public int roomNumber() {
        return roomNumber;
    }

    public void roomOcupied(){
        isAviable = false;
    }

    public void freeRoom() {
        isAviable = true;
    }

    public void setGuest(List<Guest> guests) {
//       questList.addAll((Collection<? extends Guest>) guests);
        questList.addAll(guests);
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomNumber=" + roomNumber +
                ", numberOfPersons=" + numberOfPersons +
                ", hasBathroom=" + hasBathroom +
                ", isAviable=" + isAviable +
                ", questList=" + questList +
                '}';
    }

    public void showGuestList() {
        System.out.println(questList);
    }
}
