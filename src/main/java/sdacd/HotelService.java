package sdacd;

import java.util.ArrayList;
import java.util.List;

public class HotelService {

    private Hotel hotel;

    public HotelService(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Room> getRoomsList() {
        return hotel.getRoomsList();
    }

    public List<Room> getAviableRoom() {
        List<Room> aviableRooms = new ArrayList<>();
        for (Room room : hotel.getRoomsList()) {
            if (room.isAviable()) {
                aviableRooms.add(room);
            }
        }
        return aviableRooms;
    }

    public Room getRoomByNumber(int roomNumber) {
        for (Room room : getRoomsList()) {
            if (room.roomNumber() == roomNumber) {
                return room;
            }
        }
        return null;
    }

    public void putGuestsInRoom(int roomNumber, List<Guest> guests) {
        getRoomByNumber(roomNumber).setGuest(guests);
    }


    boolean bookRoom(int roomNumber, List<Guest> guests) {
        if (!isAdult(guests)) return false;
        for (Room room : getAviableRoom()) {
            if (room.roomNumber() == roomNumber) {
                room.roomOcupied();
                putGuestsInRoom(roomNumber, guests);
            } else {
                return false;
            }
        }
        return true;
    }

//    private void setGuest(List<Guest> guests) {
//
//    }

    boolean chekOut(int roomNumber) {
        for (Room room : getRoomsList()) {
            if (room.roomNumber() == roomNumber) {
                if (room.isAviable() == true) {
                    return false;
                } else {
                    room.freeRoom();
                }
            }
        }
        return true;
    }

    private boolean isAdult(List<Guest> guests) {
        for (Guest guest : guests) {
            if (guest.getAge() >= 18) {
                return true;
            }
        }
        return false;
    }


    public void showRooms(List rooms) {
        System.out.println("Pokoje.");
        for (Object room : rooms) {
            System.out.println(room);
        }
        System.out.println();
    }
//    public void showFreeRooms(List rooms) {
//        System.out.println("Wszystkie pokoje.");
//        for(Object room : rooms) {
//            System.out.println(room);
//        }
//    }
}
