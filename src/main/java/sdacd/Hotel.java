package sdacd;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private List<Room> roomsList = new ArrayList<>();
//    private List<Room> guestList = new ArrayList<>();

    public Hotel(){
        roomsList.add(new Room(100,3,true,true));
        roomsList.add(new Room(101,4,false,true));
        roomsList.add(new Room(200,4,true,true));
        roomsList.add(new Room(201,5,false,true));
    }

    public List<Room> getRoomsList() {
        return roomsList;
    }


    //    public List<Room> getAviableRoom() {
//
//    }
}
